/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/31 23:56:20 by racherom          #+#    #+#             */
/*   Updated: 2024/11/03 20:27:33 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Zombie.hpp"
#include <string>

int	main(int argc, char **argv)
{
	Zombie	*z;
	int		i;

	if (argc < 2)
		return (0);
	z = zombieHorde(argc, argv[1]);
	if (!z)
		return (1);
	i = 0;
	while (i < argc)
		z[i++].announce();
	delete[] z;
}
