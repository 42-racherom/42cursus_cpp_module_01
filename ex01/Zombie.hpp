/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Zombie.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/31 14:41:44 by racherom          #+#    #+#             */
/*   Updated: 2024/03/31 23:47:54 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ZOMBIE_HPP
# define ZOMBIE_HPP
# include <string>

class Zombie
{
  public:
	Zombie(std::string);
	Zombie(void);
	~Zombie(void);
	void announce(void) const;
	void set_name(std::string);

  private:
	std::string name;
};

Zombie	*zombieHorde(int N, std::string name);

#endif
