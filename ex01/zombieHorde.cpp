/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   zombieHorde.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/31 23:17:04 by racherom          #+#    #+#             */
/*   Updated: 2024/11/03 20:45:26 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Zombie.hpp"
#include <string>

Zombie *zombieHorde(int N, std::string name)
{
	Zombie *z;
	int i;

	z = new Zombie[N];
	if (!z)
		return (0);
	i = 0;
	while (i < N)
		z[i++].set_name(name);
	return (z);
}
