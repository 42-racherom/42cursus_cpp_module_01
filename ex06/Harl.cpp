/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Harl.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/11/03 16:36:29 by racherom          #+#    #+#             */
/*   Updated: 2024/11/03 19:54:57 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Harl.hpp"
#include <iostream>
#include <string>

Harl::Harl()
{
}

Harl::~Harl()
{
}

void Harl::debug(void)
{
	std::cout << "[ DEBUG ]" << std::endl;
	std::cout << "I love having extra bacon for my 7XL-double-cheese-triple-pickle-specialketchup burger." << std::endl;
	std::cout << "I really do !" << std::endl
				<< std::endl;
}
void Harl::info(void)
{
	std::cout << "[ INFO ]" << std::endl;
	std::cout << "I cannot believe adding extra bacon costs more money." << std::endl;
	std::cout << "You didn’t put enough bacon in my burger!" << std::endl;
	std::cout << "If you did, I wouldn’t be asking for more!";
	std::cout << std::endl
				<< std::endl;
}
void Harl::warning(void)
{
	std::cout << "[ WARNING ]" << std::endl;
	std::cout << "I think I deserve to have some extra bacon for free." << std::endl;
	std::cout << "I’ve been coming for years whereas you started working here since last month.";
	std::cout << std::endl
				<< std::endl;
}
void Harl::error(void)
{
	std::cout << "[ ERROR ]" << std::endl;
	std::cout << "This is unacceptable! I want to speak to the manager now.";
	std::cout << std::endl
				<< std::endl;
}

std::string Harl::levels[5] = {"DEBUG", "INFO", "WARNING", "ERROR", "SWITCH"};
void (Harl::*Harl::functions[4])(void) = {&Harl::debug, &Harl::info,
	&Harl::warning, &Harl::error};

int Harl::log_level(std::string level)
{
	int	i;

	i = 0;
	while (i < 5)
	{
		if (level == Harl::levels[i])
			return (i);
		i++;
	}
	return (-1);
}

void Harl::complain(std::string level)
{
	int	i;

	i = this->log_level(level);
	if (i >= 0 && i < 5)
		(this->*functions[i])();
	else
		std::cout << "[ Probably complaining about insignificant problems ]" << std::endl;
}
