/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/11/03 18:20:57 by racherom          #+#    #+#             */
/*   Updated: 2024/11/08 15:56:22 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Harl.hpp"
#include <string>

int	main(int argc, char **argv)
{
	Harl	harl;

	if (argc != 2)
		return (1);
	switch (harl.log_level(argv[1]))
	{
	case 0:
		harl.complain("DEBUG");
		__attribute__ ((fallthrough));
	case 1:
		harl.complain("INFO");
		__attribute__ ((fallthrough));
	case 2:
		harl.complain("WARNING");
		__attribute__ ((fallthrough));
	case 3:
		harl.complain("ERROR");
		__attribute__ ((fallthrough));
	case 4:
		break ;
	default:
		harl.complain(argv[1]);
	}
}
