/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   StreamReplacer.cpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/10/24 19:00:46 by racherom          #+#    #+#             */
/*   Updated: 2024/11/08 15:40:14 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "StreamReplacer.hpp"
#include <iostream>
#include <string>

StreamReplacer::StreamReplacer(std::istream &in, std::ostream &out,
		const std::string &s1, const std::string &s2, const int cap)
	: _s1(s1), _s2(s2), _in(in), _out(out), _cap(cap), _count(0)
{
}

StreamReplacer::~StreamReplacer()
{
}

size_t StreamReplacer::handle_chunk(const std::string &chunk, size_t &i)
{
	size_t p1, p2, l1, l2;
	if (!i)
		return (0);
	p1 = 0;
	l1 = i;
	p2 = i;
	l2 = std::min(this->_s1.length() - i, chunk.length());
	while (p1 < i)
	{
		if (!this->_s1.compare(p1, l1, this->_s1, 0, l1) &&
			!this->_s1.compare(p2, l2, chunk, 0, l2))
		{
			if (p1 > 0)
				this->_out << this->_s1.substr(0, p1);
			if (l1 + l2 < this->_s1.length())
			{
				i = l1 + l2;
				return (chunk.length());
			}
			else
			{
				this->_count++;
				this->_out << this->_s2;
				i = 0;
				return (l2);
			}
		}
		p1++;
		l1--;
		p2++;
		if (l2 < chunk.length())
			l2++;
	}
	this->_out << this->_s1.substr(0, i);
	i = 0;
	return (0);
}
int StreamReplacer::replace()
{
	size_t	pos;
	size_t	found;
	size_t	match_part;

	char *buf(new char[this->_cap]);
	match_part = 0;
	while (this->_in.read(buf, this->_cap) || this->_in.gcount() > 0)
	{
		std::string chunk(buf, this->_in.gcount());
		pos = this->handle_chunk(chunk, match_part);
		while (pos < chunk.length())
		{
			found = this->find_partial_match(chunk, pos);
			if (found > pos)
				this->_out << chunk.substr(pos, found - pos);
			if (found == chunk.length())
				break;
			pos = found + _s1.length();
			if (pos <= chunk.length())
			{
				this->_out << _s2;
				this->_count++;
			}
		}
		if (pos > chunk.length())
			match_part = chunk.length() - found;
	}
	delete[] buf;
	return (1);
}

size_t StreamReplacer::find_partial_match(const std::string &chunk, size_t i)
{
	size_t	length;

	length = std::min(this->_s1.length(), chunk.length() - i);
	while (i < chunk.length())
	{
		if (chunk.compare(i, length, this->_s1, 0, length) == 0)
			return (i);
		length = std::min(this->_s1.length(), chunk.length() - ++i);
	}
	return (chunk.length());
}

size_t StreamReplacer::get_count(void)
{
	return (this->_count);
}
