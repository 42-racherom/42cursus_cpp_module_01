/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   StreamReplacer.hpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/10/24 18:38:31 by racherom          #+#    #+#             */
/*   Updated: 2024/11/03 22:48:56 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STREAMREPLACER_HPP
# define STREAMREPLACER_HPP
# include <iostream>

class StreamReplacer
{
  private:
	std::string _s1;
	std::string _s2;
	std::istream &_in;
	std::ostream &_out;
	const int _cap;
	size_t _count;
	size_t find_partial_match(const std::string &, size_t);
	size_t handle_chunk(const std::string &, size_t &);

  public:
	StreamReplacer(std::istream &, std::ostream &, const std::string &,
			const std::string &, const int);
	~StreamReplacer();
	int replace(void);
	size_t get_count(void);
};
#endif
