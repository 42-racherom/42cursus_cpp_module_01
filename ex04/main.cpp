/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/10/11 17:45:02 by racherom          #+#    #+#             */
/*   Updated: 2024/11/08 15:37:30 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "StreamReplacer.hpp"
#include <fstream>
#include <iostream>
#include <string>
#ifndef DEFAULT_BUFFER_SIZE
# define DEFAULT_BUFFER_SIZE 1024
#endif

int	usage(std::string uname)
{
	std::cerr << "usage: " << uname << " [-b buffer_size] [-o out_file] <filename> <search> <replace>\n";
	return (1);
}

int	main(int argc, char **argv)
{
	size_t	buffer_size;
	int		i;

	std::string out_filename;
	std::string filename;
	std::string s1;
	std::string s2;
	buffer_size = DEFAULT_BUFFER_SIZE;
	i = 1;
	while (i < argc)
	{
		std::string arg = argv[i];
		if ((arg == "-o" || arg == "--output") && i + 1 < argc)
			out_filename = argv[++i];
		else if (filename.empty())
			filename = arg;
		else if (s1.empty())
			s1 = arg;
		else if (s2.empty())
			s2 = arg;
		else
			return (usage(argv[0]));
		i++;
	}
	if (filename.empty() || s1.empty() || s2.empty())
		return (usage(argv[0]));
	if (out_filename.empty())
		out_filename = filename + ".replace";
	std::ifstream in_file(filename.c_str());
	if (!in_file)
	{
		std::cerr << "error: unable to open input file.\n";
		return (1);
	}
	std::ofstream out_file((filename + ".replace").c_str());
	if (!out_file)
	{
		std::cerr << "error: unable to open output file.\n";
		in_file.close();
		return (1);
	}
	StreamReplacer replacer(in_file, out_file, s1, s2, buffer_size);
	if (!replacer.replace())
	{
		std::cerr << "error: replacement process failed.\n";
		return (1);
	}
	std::cout << "replacement complete. replaced <" << s1 << "> ";
	std::cout << replacer.get_count() << " time(s) with <" << s2;
	std::cout << ">. check the file: " << out_filename << std::endl;
	return (0);
}
