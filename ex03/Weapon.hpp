/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Weapon.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <42@racherom.wtf>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/09/29 12:30:08 by rauer             #+#    #+#             */
/*   Updated: 2024/09/29 17:26:41 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WEAPON_HPP
# define WEAPON_HPP
# include <string>

class Weapon
{
  private:
	std::string type;
  public:
	Weapon(std::string);
	~Weapon();
	const std::string &getType(void);
	void setType(std::string);
};


#endif