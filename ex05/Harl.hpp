/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Harl.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/11/03 16:36:44 by racherom          #+#    #+#             */
/*   Updated: 2024/11/03 19:48:10 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HARL_HPP
# define HARL_HPP
# include <string>

class Harl
{
  private:
	void debug(void);
	void info(void);
	void warning(void);
	void error(void);
	static std::string levels[5];
	static void (Harl::*functions[4])(void);

  public:
	Harl();
	~Harl();
	static int log_level(std::string level);
	void complain(std::string level);
};
#endif
