/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Zombie.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/31 14:41:44 by racherom          #+#    #+#             */
/*   Updated: 2024/03/31 22:46:06 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ZOMBIE_HPP
# define ZOMBIE_HPP
# include <string>

class Zombie
{
  public:
	Zombie(std::string);
	~Zombie(void);
	void announce(void) const;

  private:
	std::string const name;
};

Zombie* newZombie( std::string name );
void randomChump( std::string name );

#endif
