/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/31 16:37:48 by racherom          #+#    #+#             */
/*   Updated: 2024/11/03 22:44:06 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Zombie.hpp"
#include <iostream>
#include <string>

int	main(int argc, char **argv)
{
	int	i;
	std::string	str;

	i = 1;
	while (i < argc)
	{
		str = argv[i];
		if (i++ & 1)
		{
			Zombie *z = newZombie(str);
			if (!z)
				return (1);
			std::cout << "New Zombie" << std::endl;
			z->announce();
			std::cout << "Hit" << std::endl;
			z->announce();
			delete z;
		}
		else
			randomChump(str);
	}
}
